import { Component, OnInit } from '@angular/core';
import * as json from './home.json'
import { Router } from '@angular/router';
@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  /**
    * @description Variable para almacenar textos correspondientes a la 
    *              vista
    */
   private messages: any;

  constructor(private router: Router) {
    this.messages = json.home
   }

  ngOnInit() {
  }

  /**
     * @description Método para navegar al login
     * @method login
     */
    public login(): void {
      this.router.navigate(['login'])
    }
/**
   * @description Método para navegar al registro
   * @method register
   */
  public register(): void {
    this.router.navigate(['register'])
    // this.appCtrl.getRootNav().push(ValidateExistingUserPage);
  }
}
