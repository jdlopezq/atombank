import { Component, OnInit } from '@angular/core';
import * as json from './login.json'
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service.js';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  /**
    * @description Variable para almacenar textos correspondientes a la 
    *              vista
    */
  private messages: any;
  /**
     * @description Variable para  crear el formulario de login 
     */
  private loginForm: FormGroup

  constructor(private fb: FormBuilder,
    private autoService: AuthService,
    private router: Router,
    private navController: NavController) {
    this.messages = json.login
    this.loginForm = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  ngOnInit() {
  }

  async onLogin() {
    const user = await this.autoService.onLogin(this.loginForm.value)
    if (user) {
      this.navController.navigateRoot(['/tabs-with-login'])
      // this.router.navigateByUrl('/tabs-with-login')
      this.loginForm.reset()
      console.log(user)
    }
  }

}
