import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TransferPage } from './transfer.page';
import { MaterialModule } from 'src/app/shared/material.module';
import { ComponentsModule } from 'src/app/componets/components.module';

const routes: Routes = [
  {
    path: '',
    component: TransferPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes), 
    MaterialModule,
    ComponentsModule, ReactiveFormsModule
  ],
  declarations: [TransferPage],
  entryComponents:[TransferPage]
})
export class TransferPageModule {}
