import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { UtilitiesService } from 'src/app/services/utilities.service';
import { timeout } from 'rxjs/operators';

@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.page.html',
  styleUrls: ['./transfer.page.scss'],
})
export class TransferPage implements OnInit {
  private transferData
  private qrTransferForm: FormGroup
  private succesTitle
  private accounts = [{ number: '109-098978-798' }]
  constructor(private navParams: NavParams,
    private modalCtrl: ModalController,
    private fb: FormBuilder,
    private utilities: UtilitiesService) {
    this.qrTransferForm = this.fb.group({
      account: ['', Validators.required],
      value: [''],
    })
  }

  ngOnInit() {
    this.transferData = JSON.parse(this.navParams.get('data'))
    console.log(this.navParams.get('data'))
    if (this.transferData.value!='') {
      this.qrTransferForm.controls.value.setValue(this.transferData.value)
    }
    this.qrTransferForm.controls.value.valueChanges.subscribe(data => {
      let formatNumber = this.utilities.getCurrencyValue(data)
      if (data !== formatNumber) {
        this.qrTransferForm.controls.value.setValue(formatNumber)
      }
    })
  
  }

  close() {
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

  done() {
    this.succesTitle = 'Transaccion exitosa'
  }

}
