import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { AccountsService } from 'src/app/services/accounts.service';
import { account } from 'src/app/shared/accouts.interface';
import { LoadingController } from '@ionic/angular';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-products',
  templateUrl: './products.page.html',
  styleUrls: ['./products.page.scss'],
})
export class ProductsPage implements OnInit {
  accountsForm: FormGroup
  displayedColumns = ['Nombre', 'Numero', 'Saldo'];
  infoTable = ['name', 'accountNumber', 'value']
  dataSource = new MatTableDataSource<account>();
  dataSource2 = new MatTableDataSource<account>();
  createForm=false

  constructor(private productsServ: AccountsService,
    public loadingController: LoadingController,
    private fb: FormBuilder) {
    this.accountsForm = this.fb.group({
      accountType:[],
      accountNumber: [],
      value: [],
      name: []
    })

  }



  ngOnInit() {
    this.productsServ.getAcounts('accounts').subscribe(data => {
      this.dataSource2.data = data
      console.log(data);

    })
    this.productsServ.getAcounts('savings').subscribe(data => {
      this.dataSource.data = data
      console.log(data);
    })
  }

  createAccount(){
    this.productsServ.createAccount(this.accountsForm.value, this.accountsForm.value.accountType)
  }

  highlight(row, table) {
    this.productsServ.deleteAccount(row.id, table)
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'Estamos cargando tu información',
      spinner: 'bubbles'
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

    console.log('Loading dismissed!');
  }

  create(){
    this.createForm=true
  }
  cancel(){
    this.accountsForm.reset()
    this.createForm=false
  }
}


