import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabsWithLoginPage } from './tabs-with-login.page';

describe('TabsWithLoginPage', () => {
  let component: TabsWithLoginPage;
  let fixture: ComponentFixture<TabsWithLoginPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabsWithLoginPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabsWithLoginPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
