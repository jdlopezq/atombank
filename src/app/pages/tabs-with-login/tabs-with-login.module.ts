import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TabsWithLoginPage } from './tabs-with-login.page';
import { ComponentsModule } from 'src/app/componets/components.module';
import { TabsPageRoutingModule } from './tabs-with-login-routing.module';
import { MaterialModule } from 'src/app/shared/material.module';
import { ProductsPageModule } from '../products/products.module';

const routes: Routes = [
  {
    path: '',
    component: TabsWithLoginPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TabsPageRoutingModule,
    ComponentsModule,
    MaterialModule,
    ProductsPageModule,
  ],
  declarations: [TabsWithLoginPage]
})
export class TabsWithLoginPageModule {}
