import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsWithLoginPage } from './tabs-with-login.page';
import { ProductsPage } from '../products/products.page';

const routes: Routes = [
  {
    path: '',
    component: TabsWithLoginPage,
    children: [
      {
        path: 'products',
        // component:ProductsPage,
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../products/products.module').then(m => m.ProductsPageModule)
          }
        ]
      },
      {
        path: 'transactions',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../transactions/transactions.module').then(m => m.TransactionsPageModule)
          }
        ]
      },
      {
        path: 'weather',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../weather/weather.module').then(m => m.WeatherPageModule)
          }
        ]
      } ,
      {
        path: '',
        redirectTo: '/tabs-with-login/products',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs-with-login/products',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
