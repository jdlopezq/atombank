import { Component, OnInit } from '@angular/core';
import { WeatherService } from 'src/app/services/weather.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.page.html',
  styleUrls: ['./weather.page.scss'],
})
export class WeatherPage implements OnInit {
  weather
  position
  direction
  temp
  isLoading
  constructor(private weatherServ: WeatherService,
    private geolocation: Geolocation,
    public loadingController: LoadingController) { }

  ngOnInit() {
    this.presentLoading()
    this.geolocation.getCurrentPosition().then((resp) => {

      console.log(resp);
      this.weatherServ.getCurrentWeather(resp.coords.latitude, resp.coords.longitude).subscribe((data: any) => {
        console.log(data);
        let tempKelvin
        this.weather = data.weather[0]
        this.position = data
        tempKelvin = parseInt(data.main.temp) - 273.15
        this.temp = tempKelvin.toFixed(2)
        this.dismiss()
      })
      this.weatherServ.getPlaceName(resp.coords.latitude, resp.coords.longitude).subscribe(data => {
        console.log(data);
        this.direction = data
      })

    }).catch((error) => {
      console.log('Error getting location', error);
    });



  }

  async presentLoading() {
    this.isLoading = true;
    const loading = await this.loadingController.create({
      message: 'Consultando los expertos en el clima',
      spinner: 'bubbles'
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });

 
  }
  async dismiss() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

}
