import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as json from './register.json'
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service.js';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  /**
    * @description Variable para almacenar textos correspondientes a la 
    *              vista
    */
   private messages: any;
  /**
      * @description Variable para  crear el formulario de login 
      */
  private registerForm: FormGroup
  constructor(private fb : FormBuilder,
     private router:Router,
     private authService: AuthService
    ) {
    this.messages = json.register
    this.registerForm = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
      confirmPasword:['', Validators.required]
    })
  }

  ngOnInit() {
  }

  async onRegister(){
    const user =await this.authService.onRegister(this.registerForm.value)
    if (user) {
      console.log(user);
      this.router.navigate(['home'])
    }
  }

}
