import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { JsonPipe } from '@angular/common';
import { UtilitiesService } from 'src/app/services/utilities.service';
import { ModalController } from '@ionic/angular';
import { QrModalPage } from '../qr-modal/qr-modal.page';
import {BarcodeScanner} from '@ionic-native/barcode-scanner/ngx'
import { TransferPage } from '../transfer/transfer.page';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.page.html',
  styleUrls: ['./transactions.page.scss'],
})
export class TransactionsPage implements OnInit {
  private qrForm: FormGroup
  private scannedCode=null
  private createdCode
  private generateQR
  private accounts=[{number:'109-098978-798'}]
  constructor(private fb: FormBuilder,
    private utilities: UtilitiesService,
    public modalController: ModalController, 
    private barCodescanner: BarcodeScanner) {
    this.qrForm = this.fb.group({
      originAccount: ['', Validators.required],
      open: ['', Validators.required],
      value: [''],
      reference: ['']

    })
    this.generateQR=true
  }

  ngOnInit() {
    this.qrForm.controls.open.valueChanges.subscribe(data=>{
      console.log(JSON.parse(data));
      if (JSON.parse(data)) {
        this.qrForm.controls.value.enable()
      }else{
        this.qrForm.controls.value.disable()
      }
    });
    this.qrForm.controls.value.valueChanges.subscribe(data=>{
      let formatNumber = this.utilities.getCurrencyValue(data)
      if (data !== formatNumber) {
        this.qrForm.controls.value.setValue(formatNumber)
      }
    })
  }

  async createCode() {
    this.createdCode=JSON.stringify(this.qrForm.value)
      const modal = await this.modalController.create({
        component: QrModalPage,
        componentProps:{data:this.qrForm.value}
      });
      return await modal.present();
    }
  
async scanCode(){
this.barCodescanner.scan().then(barcodedata=>{
  console.log(barcodedata);
  if (!barcodedata.cancelled) {
     this.transfer(barcodedata.text)
  this.scannedCode=barcodedata.text
  }

})
}
async transfer(data){
  const modal = await this.modalController.create({
    component: TransferPage,
    componentProps:{data:data}
  });
  return await modal.present();
}
changemode(){
  this.generateQR=!this.generateQR
}
}
