import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TransactionsPage } from './transactions.page';
import { ComponentsModule } from 'src/app/componets/components.module';
import { MaterialModule } from 'src/app/shared/material.module';
import { DirectivesModule } from 'src/app/directives/directives.module';
import { NgxQRCodeModule } from 'ngx-qrcode2';


const routes: Routes = [
  {
    path: '',
    component: TransactionsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ComponentsModule,
    ReactiveFormsModule,
    MaterialModule,
    DirectivesModule,
    NgxQRCodeModule,
  ],
  declarations: [TransactionsPage],

})
export class TransactionsPageModule {}
