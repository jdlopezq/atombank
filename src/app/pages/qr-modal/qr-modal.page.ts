import { Component, OnInit, Input } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-qr-modal',
  templateUrl: './qr-modal.page.html',
  styleUrls: ['./qr-modal.page.scss'],
})
export class QrModalPage implements OnInit {
  @Input() data: string;
  createdCode
  constructor(private navParams: NavParams, 
   private modalCtrl:ModalController) { }

  ngOnInit() {
    console.log(this.navParams.get('data'));
    this.createQR()
  }

 async createQR(){
    let data=this.navParams.get('data')
   this.createdCode=await JSON.stringify(data)
  }

  close(){
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }
}
