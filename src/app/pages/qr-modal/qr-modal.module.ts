import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { QrModalPage } from './qr-modal.page';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { ComponentsModule } from 'src/app/componets/components.module';
import { MaterialModule } from 'src/app/shared/material.module';

const routes: Routes = [
  {
    path: '',
    component: QrModalPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    NgxQRCodeModule, ComponentsModule,
    MaterialModule
  ],
  declarations: [QrModalPage],
  entryComponents:[QrModalPage]
})
export class QrModalPageModule {}
