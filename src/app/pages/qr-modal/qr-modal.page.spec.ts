import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QrModalPage } from './qr-modal.page';

describe('QrModalPage', () => {
  let component: QrModalPage;
  let fixture: ComponentFixture<QrModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QrModalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QrModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
