import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators'
import { account } from '../shared/accouts.interface'


@Injectable({
  providedIn: 'root'
})
export class AccountsService {
  private accountCollection: AngularFirestoreCollection<account>
  private accounts: Observable<account[]>
  constructor(private db: AngularFirestore) {
  this.setData('accounts')
  }
setData(collection){
  this.accountCollection = this.db.collection<account>(collection)
    this.accounts = this.accountCollection.snapshotChanges().pipe(map(data => {
      return data.map(a => {
        const datos = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...datos };
      })
    }))
}

getAcounts(collection){
  this.setData(collection)
  return this.accounts;
}
getAccount(id:string){
  return this.accountCollection.doc<account>(id).valueChanges();
}

updateAccount(account:account, id:string){
return this.accountCollection.doc(id).update(account)
}

createAccount(account:account, collection){
  this.setData(collection)
 return this.accountCollection.add(account)
}

deleteAccount(id:string, collection){
  this.setData(collection)
return this.accountCollection.doc(id).delete()
}


}
