import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  APIkey = '507fe77a4cc944dfd3fa31425b3a29d8';
  APIkeyName='c1d1a38353704167984e5795928283b4'
  URL = 'https://api.openweathermap.org/data/2.5/weather?';
  urlName='https://api.opencagedata.com/geocode/v1/json?q='

  constructor(private http: HttpClient) { }

  getCurrentWeather(lat, lon) {
    return this.http.get(`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&APPID=${this.APIkey}&lang=es&units=metric'`).pipe(map((response: Response) => {
      return response;
    }))
  }
  getPlaceName(lat, lon) {
    return this.http.get(this.urlName + lat+'+'+lon + '&key=' + this.APIkeyName).pipe(map((response: any) => {
      return response.results[0].formatted;
    }))
  }
}
