import { Injectable } from '@angular/core';
import { CurrencyPipe } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class UtilitiesService {

  constructor(private currencyPipe: CurrencyPipe,) { }


  public getCurrencyValue(value: string) {
    if (value!=null && value!="") {
      let number = this.removeCurrencyValue(value)
      let formatNumber=(this.currencyPipe.transform(number, 'COP', 'symbol')).replace('$','')
       return formatNumber.replace(/\s/g, '');
    }
  }

  /**
   * @description Método que permite remover el formato de un número
   *              ej. 1,200,000 => 1200000.
   * @method getDataWithoutFormat
   */
  public removeCurrencyValue(value): any {
    return value.replace(/[^0-9]/g, '');
  }
}
