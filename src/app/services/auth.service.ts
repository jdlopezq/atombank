import { Injectable } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth'
@Injectable({
  providedIn: 'root'
})
export class AuthService {
public isLogged: any=false
  constructor(public afAuth:AngularFireAuth) { 
    afAuth.authState.subscribe(user=>{
      this.isLogged=user
    })
  }

//login 

async onLogin(user){
  try{
    return await this.afAuth.auth.signInWithEmailAndPassword(user.email, user.password)
  }catch(error){
    console.log('error en el Loggin: ', error)
  }
}

//Register

async onRegister(user){
  try {
    return await this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.password)
  } catch (error) {
    console.log('error en el registro de usuario: ', error)
  }
}

}
