import { Directive, HostListener, ElementRef, Renderer } from '@angular/core';

@Directive({
  selector: '[BlockCopyPaste]'
})
export class BlockCopyPasteDirective {

  constructor(el?: ElementRef, renderer?: Renderer) {
    var events = 'cut copy paste';
    events.split(' ').forEach(e => 
    renderer.listen(el.nativeElement, e, (event) => {
      event.preventDefault();
      })
    );

  }


  @HostListener('paste', ['$event']) blockPaste(e: KeyboardEvent) {
    e.preventDefault();
  }

  @HostListener('copy', ['$event']) blockCopy(e: KeyboardEvent) {
    e.preventDefault();
  }

  @HostListener('cut', ['$event']) blockCut(e: KeyboardEvent) {
    e.preventDefault();
  }
}
