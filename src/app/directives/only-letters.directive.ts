import { Directive, HostListener, ElementRef } from '@angular/core';

@Directive({
  selector: '[OnlyLetters]'
})
export class OnlyLettersDirective {
  
  
  // @HostListener('keydown',['$event']) onKeyDown(event) {
  //   console.log(event);
  //   let transformedInput = (event.key).replace(/[^A-Za-zÀ-ž]/g, '');
  //   if(transformedInput !== event.key)
  //     event.preventDefault();
  // }

  constructor(private _el?: ElementRef) { }

  @HostListener('input', ['$event']) onInputChange(event) {
    const initalValue = this._el.nativeElement.value;
    this._el.nativeElement.value = initalValue.replace(/[^A-Za-zÀ-ž]*/g, '');
    if ( initalValue !== this._el.nativeElement.value) {
      event.stopPropagation();
    }
  }
}
