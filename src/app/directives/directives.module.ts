import { NgModule } from '@angular/core';
import { OnlyNumberDirective } from './only-number.directive';
import { BlockCopyPasteDirective } from './block-copy-paste.directive';
import { OnlyLettersDirective } from './only-letters.directive';
import { AlphaNumericDirective } from './alpha-numeric.directive';

var directives = [
OnlyNumberDirective,
BlockCopyPasteDirective,
OnlyLettersDirective,
AlphaNumericDirective
];
@NgModule({
	declarations: directives,
	imports: [], 
	exports: directives
})
export class DirectivesModule {}
