import { NgModule, LOCALE_ID } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {AngularFireModule} from '@angular/fire';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { Geolocation } from '@ionic-native/geolocation/ngx'; 
import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';
import {environment} from './../environments/environment'
import { CurrencyPipe, registerLocaleData } from '@angular/common';
import _default from '@angular/common/locales/es-CO'
import { QrModalPage } from './pages/qr-modal/qr-modal.page';
import { QrModalPageModule } from './pages/qr-modal/qr-modal.module';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { TransferPageModule } from './pages/transfer/transfer.module';
import { TransferPage } from './pages/transfer/transfer.page';
import { HttpClientModule } from '@angular/common/http';
registerLocaleData(_default, 'es-CO')

@NgModule({
  declarations: [AppComponent],
  entryComponents: [QrModalPage, TransferPage],
  imports: [BrowserModule, 
    IonicModule.forRoot(), 
    AppRoutingModule, 
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    AngularFirestoreModule,
    NgxQRCodeModule,
    QrModalPageModule,
    TransferPageModule,
    HttpClientModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: LOCALE_ID, useValue: "es-CO" },
    CurrencyPipe,
    BarcodeScanner,
    NativeGeocoder,
    Geolocation,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
