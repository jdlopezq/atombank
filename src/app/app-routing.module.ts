import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import {AuthGuard} from './guards/auth.guard'
const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./pages/tabs/tabs.module').then(m => m.TabsPageModule)
  },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'offices', loadChildren: './pages/offices/offices.module#OfficesPageModule' },
  { path: 'about-us', loadChildren: './pages/about-us/about-us.module#AboutUsPageModule' },
  { path: 'home', loadChildren: './pages/home/home.module#HomePageModule' },
  { path: 'register', loadChildren: './pages/register/register.module#RegisterPageModule' },
  { path: 'tabs-with-login', loadChildren: './pages/tabs-with-login/tabs-with-login.module#TabsWithLoginPageModule',
  //  canActivate:[AuthGuard]
  },
  { path: 'products', loadChildren: './pages/products/products.module#ProductsPageModule' },
  { path: 'transactions', loadChildren: './pages/transactions/transactions.module#TransactionsPageModule' },
  { path: 'inscription', loadChildren: './pages/inscription/inscription.module#InscriptionPageModule' },
  { path: 'qr-modal', loadChildren: './pages/qr-modal/qr-modal.module#QrModalPageModule' },
  { path: 'transfer', loadChildren: './pages/transfer/transfer.module#TransferPageModule' },
  { path: 'weather', loadChildren: './pages/weather/weather.module#WeatherPageModule' }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
