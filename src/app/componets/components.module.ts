import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { MaterialModule } from 'src/app/shared/material.module';
import { GlobalBannerComponent } from './global-banner/global-banner.component';


@NgModule({
  declarations: [
   GlobalBannerComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    MaterialModule
  ],
  exports: [
GlobalBannerComponent
  ]
})
export class ComponentsModule { }
