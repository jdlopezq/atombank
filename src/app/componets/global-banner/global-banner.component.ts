import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-global-banner',
  templateUrl: './global-banner.component.html',
  styleUrls: ['./global-banner.component.scss'],
})
export class GlobalBannerComponent implements OnInit {


  /**
   * @description Variable para almacenar textos correspondientes a la 
   *              vista
   */
  private messages: any;

  /**
   * @description Variable para el título
   */
  @Input() title: string;

  /**
   * @description Variable mostrar o no el logo
   */
  @Input() logo: boolean = true;

  @Input() logoutBtn: boolean

  @Input() backButton: boolean 

  constructor(private authService: AuthService, 
    private router: Router,
    private afAuth: AngularFireAuth) {

   }

  ngOnInit() {}

  logout(){
this.afAuth.auth.signOut()
console.log('Sesion finalizada');

this.router.navigate([''])
  }
}
