// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyC3jW4c2P2cKBYG2B_tQDLVF4lXQmik3p0",
    authDomain: "atombank-5a260.firebaseapp.com",
    databaseURL: "https://atombank-5a260.firebaseio.com",
    projectId: "atombank-5a260",
    storageBucket: "atombank-5a260.appspot.com",
    messagingSenderId: "771064531084",
    appId: "1:771064531084:web:5ca6ed22c82a443a4493a9",
    measurementId: "G-JGD84Y2MRH"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
